# OBS Scripts

## Rectangle area selector for KDE Wayland

This script allows you to workaround the current Wayland limitation in KDE session, that you cannot record the rectangular screen region (see https://bugs.kde.org/show_bug.cgi?id=469779 for Spectacle support).  
In X11 session it was very convenient with Simple Screen Recorder.  
With this script, you can achieve kinda similar thing.  

![Place over tray to record only that area.png](./Area%20Selection%20Window.png)

### Setup:

Assumed you use KDE (script works with kwin).  
Install OBS studio. It is required that it has `obs-websocket` plugin. It is built-in, but if you use Arch Linux, install `obs-studio-git`, because currently (August 2023) the `obs-studio` misses obs-websocket plugin (see https://bugs.archlinux.org/task/76710).  
Enable Plugin in OBS -> Tools -> Websocket Server Settings.  
Generate the password, and fill it in the `~/config.toml`  

```
[connection]
host = "localhost"
port = 4455
password = "mystrongpass"
```

### Usage:

- Launch OBS (script communicates with it, so it must be launched when you run it).
- Launch the script: `python rectangle-area-selector-for-obs.py`.
- The window will appear. Resize it to wanted height and width, and move it to the position on the screen where you want it to be recordered.
- Press the "Set this region in OBS" button.
- In OBS you will see the canvas is sized and moved according to the window.
- You can now close the window, and record/stream with OBS as you do normally. 
