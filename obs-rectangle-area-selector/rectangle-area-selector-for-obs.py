#!/usr/bin/env python3

# Author: Andrew Shark

# This script allows you to record an arbitrary screen region in kde wayland with obs.

import sys
from PyQt6.QtWidgets import QLabel, QApplication, QWidget, QPushButton, QMainWindow, QVBoxLayout
from PyQt6.QtCore import Qt
import subprocess
from datetime import datetime
import os
import obsws_python as obs


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()

        self.scene_name = "Workspace"
        self.source_name = "Screen Capture PipeWire"
        self.screen_scale = 1.25  # see in system settings

        self.setWindowTitle("Rectangle Area Selector")  # using this title, the kwin script determines window geometry
        layout = QVBoxLayout()
        label = QLabel("Resize this window and place it to the region you want to record")
        label.setMinimumWidth(1)
        button = QPushButton("Set this region in OBS")
        button.setMinimumWidth(1)
        buttonReset = QPushButton("Reset OBS canvas and source transform")
        buttonReset.setMinimumWidth(1)
        layout.addWidget(label, alignment=Qt.AlignmentFlag.AlignHCenter)
        layout.addWidget(button)
        layout.addWidget(buttonReset)

        widget = QWidget()
        widget.setLayout(layout)
        self.setCentralWidget(widget)

        button.clicked.connect(self.on_button_click)
        buttonReset.clicked.connect(self.reset_canvas_and_transform)

        self.x = 0
        self.y = 0
        self.w = 0
        self.h = 0

        self.cl = obs.ReqClient()

    def get_window_geometry_from_kwin(self):
        datetime_now = datetime.now()

        script = os.path.abspath("get_window_geometry_from_kwin.js")

        reg_script_number = subprocess.run("dbus-send --print-reply --dest=org.kde.KWin \
                            /Scripting org.kde.kwin.Scripting.loadScript \
                            string:" + script + " | awk 'END {print $2}'",
                                           capture_output=True, shell=True).stdout.decode().split("\n")[0]

        subprocess.run("dbus-send --print-reply --dest=org.kde.KWin /" + reg_script_number + " org.kde.kwin.Script.run",
                       shell=True, stdout=subprocess.DEVNULL)
        subprocess.run("dbus-send --print-reply --dest=org.kde.KWin /" + reg_script_number + " org.kde.kwin.Script.stop",
                       shell=True, stdout=subprocess.DEVNULL)  # unregister number

        since = str(datetime_now)

        msg = subprocess.run("journalctl _COMM=kwin_wayland -o cat --since \"" + since + "\"",
                             capture_output=True, shell=True).stdout.decode().rstrip().split("\n")
        msg = [el.lstrip("js: ") for el in msg if not el.startswith("kwin_screencast")]

        geometry_str = msg[0]
        if not geometry_str.startswith("QRectF"):
            return 1
        geometry_str = geometry_str.removeprefix("QRectF(") .removesuffix(")")
        self.x, self.y, self.w, self.h = [int(el) for el in geometry_str.split(", ")]
        print(self.x, self.y, self.w, self.h)

    def set_obs_canvas_size_and_source_position_in_canvas(self):
        base_height, base_width, fps_denominator, fps_numerator, output_height, output_width = self.cl.get_video_settings().base_height, self.cl.get_video_settings().base_width, self.cl.get_video_settings().fps_denominator, self.cl.get_video_settings().fps_numerator, self.cl.get_video_settings().output_height, self.cl.get_video_settings().output_width
        self.cl.set_video_settings(fps_numerator, fps_denominator, int(self.w * self.screen_scale), int(self.h * self.screen_scale), int(self.w * self.screen_scale), int(self.h * self.screen_scale))

        id_of_screen_capture_item = self.cl.get_scene_item_id(self.scene_name, "Screen Capture PipeWire").scene_item_id
        # tr = self.cl.get_scene_item_transform(self.scene_name, id_of_screen_capture_item).scene_item_transform
        # print(tr)
        tr_new = {}
        tr_new['positionX'] = self.x * -1 * self.screen_scale
        tr_new['positionY'] = self.y * -1 * self.screen_scale
        self.cl.set_scene_item_transform(self.scene_name, id_of_screen_capture_item, tr_new)

    def on_button_click(self):
        self.get_window_geometry_from_kwin()
        self.set_obs_canvas_size_and_source_position_in_canvas()

    def reset_canvas_and_transform(self):
        base_height, base_width, fps_denominator, fps_numerator, output_height, output_width = self.cl.get_video_settings().base_height, self.cl.get_video_settings().base_width, self.cl.get_video_settings().fps_denominator, self.cl.get_video_settings().fps_numerator, self.cl.get_video_settings().output_height, self.cl.get_video_settings().output_width
        self.cl.set_video_settings(fps_numerator, fps_denominator, 1920, 1080, 1920, 1080)

        id_of_screen_capture_item = self.cl.get_scene_item_id(self.scene_name, "Screen Capture PipeWire").scene_item_id
        print(id_of_screen_capture_item)
        tr = self.cl.get_scene_item_transform(self.scene_name, id_of_screen_capture_item).scene_item_transform
        print(tr)

        tr_new = {}
        tr_new['positionX'] = 0.0
        tr_new['positionY'] = 0.0
        tr_new['scaleX'] = 1920 / tr['sourceWidth']
        tr_new['scaleY'] = 1080 / tr['sourceHeight']

        self.cl.set_scene_item_transform(self.scene_name, id_of_screen_capture_item, tr_new)

app = QApplication(sys.argv)
window = MainWindow()
window.show()
app.exec()
